import os

# Simple Minecraft .mca world file deleter by af-a1997. Useful to reset chunks. Currently deletes only [Overworld] and [The Nether] files.

# IMPORTANT! Note that a .mca file contains multiple chunks, be sure to check if you haven't built anything you don't want to lose within the region covered by the .mca files.

# Input the name of the directory where your world's files is (and don't forget to add a slash at the end, unless you're placing this script on the same directory as your level.dat and the world's subdirectories, in that case you leave this variable as an empty string):
world_name = "world/"

# Keep the following .mca files (below are examples from my world):
# IMPORTANT! Remember that commas are used ONLY to separate entries in the list. Use the dots ONLY for the numbers of the .mca files, as shown below.
list_of_mcas_overworld = ["0.0","0.1","1.0","1.1", "1.-5","2.6","2.8","-1.0","-1.-1","-2.0","-2.-1","-2.2","-2.-2","-3.0", "-3.-14","-3.-19"]
list_of_mcas_nether = ["0.0","0.-1","-1.0","-1.-1"]
list_of_mcas_end = None
# Input [None] if you don't want to cleanup a dimension, however, the program will stop.

# Dimensions' base directories:
# ·  0: the root of your world's files.
# ·  1: where The Nether's files are.
# ·  2: where The End's files are.
# · 3+: worlds from mods are at the /dimensions directory of your world.
mc_wld_dims = ("", "DIM-1/", "DIM1/")
mc_wld_dims_labels = ("[Overworld]", "[The Nether]", "[The End]")

# Dimensions' sub-directories:
mc_wld_dims_subdirs = ("entities", "poi", "region")

# Scan each dimension's list for deleting .mca files NOT in said dimension:
for d, selected_dimension in enumerate(mc_wld_dims):
    # Auxiliar list is used to check dimension the program is walking through:
    if selected_dimension == mc_wld_dims[0]:
        aux_list = list_of_mcas_overworld
    elif selected_dimension == mc_wld_dims[1]:
        aux_list = list_of_mcas_nether
    elif selected_dimension == mc_wld_dims[2]:
        aux_list = list_of_mcas_end
    else:
        print("Program halted due to an error with selected dimension (out of bounds).")
        exit()

    if aux_list == None:
        print("You didn't select any .mca files to delete in " + mc_wld_dims_labels[d] + ", the process will stop here.")
        exit()
    else:
        # Add common prefix and extension to each .mca entry in the auxiliar list that was just made:
        for e, add_ext in enumerate(aux_list):
            aux_list[e] = "r." + add_ext + ".mca"

    for selected_subdir in mc_wld_dims_subdirs:
        # REMEMBER to paste the world directory on the same directory as this script BEFORE running! AND ensure the names match (change "world" with the name of the directory where your world is located).
        # If you want to put the script on the same directory as your world's files, remove ONLY the "world" keyword from the first string, and run the script with the directory being set to that of the world.
        selected_work_dir = "./" + world_name + selected_dimension + selected_subdir + "/"

        for file in os.listdir(selected_work_dir):
            mca_fn = os.fsdecode(file)

            # If the .mca to delete is NOT in the list, delete it:
            if mca_fn not in aux_list:
                fn_loc_full = os.path.join(selected_work_dir, mca_fn)

                # Some of the directories may have .mca files that others don't. So, avoid crash from files that don't exist by checking their existance before deletion:
                if os.path.isfile(fn_loc_full):
                    os.remove(fn_loc_full)
                    print("DELETED: <" + fn_loc_full + ">")

                continue
            # .mca files in the list are ignored.
            else:
                # Uncomment the following line if you need to see preserved .mca files being reported in console:

                # print("KEPT: <" + os.path.join(selected_work_dir, mca_fn) + ">")

                continue