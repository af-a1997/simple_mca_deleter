# Simple Minecraft `.mca` deleter

I made a simple script in Python for myself to help me clear up chunks in my *Minecraft* world quickly, this would essentially make more resources renewable, and reduce the world's total size even by a bit. This is for *Java Edition* of the game.

> :warning: Ensure you have Python installed to run the script.

# How to use
1) Download the `mca_deleter.py` file, you can place it anywhere, but for ease of use, put it on the `/saves` directory of where the game is installed, this directory is where your worlds are stored. Open it with your text editor of choice, you can also make things easier by using an IDE of your choosing and make of `/saves` a workspace to run the script.
2) `cd` to `/saves` in a Terminal, and either temporarily rename your world to `world`, or enter its name on the `world_name` variable at line 8, while keeping the slash at the end, or leave the variable as an empty string **without** the slash if the script will be placed on the same location as the `level.dat` of your world.
3) This script doesn't detect the `.mca` files to delete, **you have to define them yourself**, an example is provided at the `list_of_mcas_overworld` variable, where you enter the names of the `.mca` files in your `/region` directory that you want to **keep** (**all others will be DELETED!**) in the same fashion I did on the example (excluding the extension and the `r.` prefix). To know the files you want to keep, press `F3` while playing in your own world, and look at the left on the chunk information, you'll eventually spot the `.mca` file name corresponding to the chunk you're at, if you don't want to reset that chunk, add the file name to the aforementioned variable.
4) Save the changes and run.

# Roadmap

- [X] Basic functionality.
- [X] Support [The Nether].
- [X] Support [The End].
- [ ] Support mod regions (TBA).
  * Planned to be [The Twilight Forest] and [The Aether] since I have them in my world.
- [ ] Make it possible to skip a dimension.
- [ ] Use Tkinter to make it interactive, and possibility to remember preferences.
- [ ] Add posibility to detect the files, and use checklist to let the user quickly pick.
- [ ] If list is implemented, also gather modification timestamp and size to make it easier for the user to distinguish which files they may want to delete.

For any bug or feature request, feel free to file an issue, I might be able to do something, but I can't guarantee it. Anyway, I hope this little script will be useful to you.